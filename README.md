# micromark-extension-tooltip

[![pipeline status](https://gitlab.com/bmitzkus/micromark-extension-tooltip/badges/main/pipeline.svg)](https://gitlab.com/bmitzkus/micromark-extension-tooltip/-/commits/main)
[![coverage report](https://gitlab.com/bmitzkus/micromark-extension-tooltip/badges/main/coverage.svg)](https://gitlab.com/bmitzkus/micromark-extension-tooltip/-/commits/main)

**[micromark][]** extension to support tooltip syntax.

Standard markdown syntax does not include tooltips. This package provides a markdown syntax extension for rendering tooltips.

## When to use this
If you’re using [`micromark`][micromark] or
[`mdast-util-from-markdown`][from-markdown], use this package.
Alternatively, if you’re using **[remark][]**, use
[`remark-tooltips`][remark-tooltips].

## Install
```sh
npm i micromark-extension-tooltip
```

## Usage
Say we have the following file, `example.md`:
```markdown
[Fasting]{The willful refrainment from eating and sometimes drinking} is linked to various
[positive health effects](https://en.wikipedia.org/wiki/Fasting#Health_effects).
```

and the following js script `example.js`:
```js
import fs from 'fs'
import {micromark} from 'micromark'
import {tooltip, tooltipHtml} from 'micromark-extension-tooltip'

const output = micromark(fs.readFileSync('example.md'), {
  extensions: [tooltip({inlineNotes: true})],
  htmlExtensions: [tooltipHtml]
})

console.log(output)
```

running `node example` yields:

```html
<p>
  <span class="tooltip">
    <span class="tooltip-text">Fasting</span>
    <span class="tooltip-note">The willful refrainment from eating and sometimes drinking</span>
  </span> is linked to various
  <a href="https://en.wikipedia.org/wiki/Fasting#Health_effects">positive health effects</a>.
</p>
```

Rendering the tooltips can be controlled via the css classes. For instance, using the following css, tooltips texts will be underlined and marked with a questionmark. On mouse hover, the tooltip note will be displayed.

```css
span.tooltip {
	text-decoration: underline dotted #008888;
	cursor: help;
	position: relative;
}

span.tooltip::after {
  content: "?";
  font-weight: bold;
  display: inline-block;
  transform: translate(0, -0.5em);
  font-size: 75%;
  color: #008888;
  margin-left: 3px;
}

span.tooltip span[class=tooltip-note] {
	display: none;
}

span.tooltip:hover span[class=tooltip-note] {
	display: block;
  box-sizing: border-box;
	position: absolute;
	padding: 0.2em;
	z-index: 100;
	color: #000;
	background-color: #92cbc5;
	border: solid 1px #008888;
	border-radius: 0.2em;
  max-width: 450px;
  top: 1.1em;
  left: 0;
  z-index: 1;
}
```

Rendering can be customized by providing a different `footnoteHtml` object (see [code](dev/lib/html.js)).

## Acknowledgement
This package is inspired by the great work of [@wooorm](https://github.com/wooorm/), mainly his [micromark-extension-footnote](https://github.com/micromark/micromark-extension-footnote) package.

<!-- Definitions -->
[micromark]: https://github.com/micromark/micromark
[remark-tooltips]: https://gitlab.com/bmitzkus/remark-tooltips
[remark]: https://github.com/remarkjs/remark
[from-markdown]: https://github.com/syntax-tree/mdast-util-from-markdown
