import test from 'tape'
import {micromark} from 'micromark'
import {tooltip as syntax, tooltipHtml as html} from '../dev/index.js'

/**
  @param {string} md
  @returns {string}
*/
function parse(md) {
  return micromark(md, {
    extensions: [syntax()],
    htmlExtensions: [html]
  })
}

test('markdown -> html (micromark)', (t) => {
  t.deepEqual(
    parse('A paragraph.[text]{note}'),
    '<p>A paragraph.<span class="tooltip"><span class="tooltip-text">text</span><span class="tooltip-note">note</span></span></p>',
    'should support calls w notes'
  )
  t.deepEqual(
    parse('A paragraph.[text]{}'),
    '<p>A paragraph.[text]{}</p>',
    'should ignore calls w/o notes'
  )
  t.deepEqual(
    parse('A paragraph.[]{note}'),
    '<p>A paragraph.[]{note}</p>',
    'should ignore notes w/o calls'
  )
  t.deepEqual(
    parse('A paragraph.[te[xt]{note}'),
    '<p>A paragraph.[te<span class="tooltip"><span class="tooltip-text">xt</span><span class="tooltip-note">note</span></span></p>',
    'should not support l bracket in tooltip text'
  )
  t.deepEqual(
    parse('A paragraph.[te]xt]{note}'),
    '<p>A paragraph.[te]xt]{note}</p>',
    'should not support r bracket in tooltip text'
  )
  t.deepEqual(
    parse('A paragraph.[text]{no{te}'),
    '<p>A paragraph.[text]{no{te}</p>',
    'should not support l brace in tooltip text'
  )
  t.deepEqual(
    parse('A paragraph.[text]{no}te}'),
    '<p>A paragraph.<span class="tooltip"><span class="tooltip-text">text</span><span class="tooltip-note">no</span></span>te}</p>',
    'should not support r brace in tooltip text'
  )
  t.end()
})
