/**
 * @typedef {import('micromark-util-types').Extension} Extension
 * @typedef {import('micromark-util-types').Token} Token
 * @typedef {import('micromark-util-types').Tokenizer} Tokenizer
 * @typedef {import('micromark-util-types').Exiter} Exiter
 * @typedef {import('micromark-util-types').State} State
 */

import assert from 'assert'
import {markdownLineEnding} from 'micromark-util-character'
import {codes} from 'micromark-util-symbol/codes.js'
import {constants} from 'micromark-util-symbol/constants.js'
import {types} from 'micromark-util-symbol/types.js'

/**
 * @returns {Extension}
 */
export function tooltip() {
  const tooltip = {tokenize: tokenizeTooltip}
  /** @type {Extension} */
  return {
    text: {[codes.leftSquareBracket]: tooltip}
  }
}

/** @type {Tokenizer} */
function tokenizeTooltip(effects, ok, nok) {
  let hasText = false
  let hasNote = false

  return start

  /** @type {State} */
  function start(code) {
    assert(code === codes.leftSquareBracket, 'expected `[`')
    effects.enter('tooltip')
    effects.enter('tooltipTextMarker')
    effects.consume(code)
    effects.exit('tooltipTextMarker')
    return textStart
  }

  /** @type {State} */
  function textStart(code) {
    effects.enter('tooltipTextString')
    effects.enter(types.chunkString, {contentType: constants.contentTypeString})
    return text(code)
  }

  /** @type {State} */
  function text(code) {
    if (
      code === codes.eof ||
      code === codes.leftSquareBracket ||
      markdownLineEnding(code)
    ) {
      return nok(code)
    }

    if (code === codes.rightSquareBracket) {
      if (!hasText) {
        return nok(code)
      }

      effects.exit(types.chunkString)
      effects.exit('tooltipTextString')

      return endText(code)
    }

    effects.consume(code)
    hasText = true

    return text
  }

  /** @type {State} */
  function endText(code) {
    effects.enter('tooltipTextMarker')
    effects.consume(code)
    effects.exit('tooltipTextMarker')
    return resume
  }

  /** @type {State} */
  function resume(code) {
    if (code !== codes.leftCurlyBrace) {
      return nok(code)
    }

    effects.enter('tooltipNoteMarker')
    effects.consume(code)
    effects.exit('tooltipNoteMarker')
    return noteStart
  }

  /** @type {State} */
  function noteStart(code) {
    effects.enter('tooltipNoteString')
    effects.enter(types.chunkString, {contentType: constants.contentTypeString})
    return noteData(code)
  }

  /** @type {State} */
  function noteData(code) {
    if (
      code === codes.eof ||
      code === codes.leftCurlyBrace ||
      markdownLineEnding(code)
    ) {
      return nok(code)
    }

    if (code === codes.rightCurlyBrace) {
      if (!hasNote) {
        return nok(code)
      }

      effects.exit(types.chunkString)
      effects.exit('tooltipNoteString')

      return endNote(code)
    }

    effects.consume(code)
    hasNote = true

    return noteData
  }

  /** @type {State} */
  function endNote(code) {
    effects.enter('tooltipNoteMarker')
    effects.consume(code)
    effects.exit('tooltipNoteMarker')
    effects.exit('tooltip')
    return ok
  }
}
