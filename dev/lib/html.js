/**
 * @typedef {import('micromark-util-types').HtmlExtension} HtmlExtension
 */

/** @type {HtmlExtension} */
export const tooltipHtml = {
  enter: {
    tooltip() {
      this.tag('<span class="tooltip">')
    },
    tooltipTextString() {
      this.buffer()
    },
    tooltipNoteString() {
      this.buffer()
    }
  },
  exit: {
    tooltip() {
      this.tag('</span>')
    },
    tooltipTextString() {
      const text = this.resume()
      this.tag('<span class="tooltip-text">' + text + '</span>')
    },
    tooltipNoteString() {
      const note = this.resume()
      this.tag('<span class="tooltip-note">' + note + '</span>')
    }
  }
}
